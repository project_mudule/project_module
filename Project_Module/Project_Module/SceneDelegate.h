//
//  SceneDelegate.h
//  Project_Module
//
//  Created by 程旭东 on 2021/6/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

